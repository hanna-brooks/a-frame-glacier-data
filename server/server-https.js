// Load required modules
const https = require("https");                 // http server core module
const fs = require("fs");
const path = require("path");
const express = require("express");           // web framework external module

// Set process name
process.title = "networked-aframe-server";

// Get port or default to 8080
const port = process.env.PORT || 8080;

// Setup and configure Express http server.
const app = express();

const server = https.createServer(
    {
        key:  fs.readFileSync(path.resolve(__dirname, "key.pem")),
        cert: fs.readFileSync(path.resolve(__dirname, "cert.pem")),
        passphrase: 'xucZFkW4'
    },
    app);
app.use(express.static(path.resolve(__dirname, "..", "public")));

app.use("/node_modules", express.static(path.resolve(__dirname, "..", "node_modules")));

// Serve the example and build the bundle in development.
if (process.env.NODE_ENV === "development") {
    const webpackMiddleware = require("webpack-dev-middleware");
    const webpack = require("webpack");
    const config = require("../webpack.config");

    app.use(
        webpackMiddleware(webpack(config), {
            publicPath: "/"
        })
    );
}
// Listen on port
server.listen(port, () => {
    console.log("listening on http://localhost:" + port);
});