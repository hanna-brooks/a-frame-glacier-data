# A-Frame Representation of Lead Isotopes and Concentrations in Snow and Ice

## Purposes

This is a visual representation of the data currated for "Lead Isotopes and Concentrations in Snow and Ice: A Global Review". In total, 326 publications are mapped across the surface of the planet in order to show the spatial dispersion of collection sites. 

## Usage

In order to interact with the globe and view the database, visit: https://hanna-brooks.gitlab.io/a-frame-glacier-data/. The buttons at the top of the page enable users to alter the base map and relief. The following base maps are available:
- Sea Ice
- Winds
- NASA Day
- NASA Night

The following relief options are available:
- Bump Water
- Bump Topo
- Bump Off

The globe is rotated with a click/drag mouse motion. Data collected from ice cores are colored red, yellow indicates data from snowpacks and green shows a location where both ice and snow were analyzed. Data collected along a transect are displayed as colored lines to the best approximation of the traveled path.

## Support
If you experience issues with the code, support can be sought by emailing hanna.brooks@maine.edu.

## Authors
Written by Hanna L Brooks and Camden G Bock. Last update: 2020.

## Citation
Citation information is included in CITATION.cff

## License
Code is licensed with a MIT License. See license section for more information.