const TerserPlugin = require('terser-webpack-plugin');
const path = require("path");

module.exports = {
    entry: {
        app: {import:'./src/index.js', filename:'public/build.min.js'},
    },
    output: {
        path: __dirname,
    },
    module: {},
    optimization: {
        minimizer: [new TerserPlugin({
            extractComments: false
        })]
    },
    devServer: {
        historyApiFallback: true,
        allowedHosts: "all",
        static: {
            directory: path.resolve(__dirname, './public'),
            publicPath: '/'
        }
    },
    resolve: {
        fallback:{
            "fs":false
        },
        alias: {
            three: 'super-three'
        }
    }

};
