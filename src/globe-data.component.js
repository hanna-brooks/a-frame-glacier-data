const THREE = window.THREE
    ? window.THREE // Prefer consumption from global THREE, if exists
    : {
        Color,
        LineBasicMaterial,
        LineSegments,
        Mesh,
        MeshPhongMaterial,
        SphereGeometry,
        TextureLoader
    };

AFRAME.registerComponent('globe-data-loader', {
    schema: {
        corePath: {'type': 'string', default: 'datasets/cores.csv'},
        traversePath: {'type': 'string', default: 'datasets/traverses.csv'},
        deltaRad: {'type': 'number', default:.1},
        deltaScale: {'type': 'number', default:1.025}
    },
    init: function(){
        document.addEventListener('keydown', e => this.keydown(e));

        this.el.setAttribute('globe', {
            arcStartLat: d => +d.startlat,
            arcStartLng: d => +d.startlng,
            arcEndLat: d => +d.endlat,
            arcEndLng: d => +d.endlng,
            arcColor: d=> d.color,
            arcAltitude: 0,

            pointColor: d=> d.color,
            pointAltitude: d=> 2e-2,
            pointRadius:0.1,
            pointsMerge: true
        });

        const coreParse = ({ Latitude, Longitude, Color, Elevation}) => ({ lat: +Latitude, lng: +Longitude, color: Color, alt:+Elevation});
        const traverseParse = ({ StartLat, StartLng, EndLat, EndLng, Color}) => ({ startlat: +StartLat, startlng: +StartLng, endlat: +EndLat, endlng: +EndLng, color:Color});
        Promise.all([
            fetch(this.data.corePath).then(res => res.text())
                .then(d => d3.csvParse(d, coreParse)),
            fetch(this.data.traversePath).then(res => res.text())
                .then(d => d3.csvParse(d, traverseParse))
        ]).then(([cores,traverses]) => {

            this.el.setAttribute('globe', {
                pointsData: cores,
                arcsData: traverses
            });
        });
    },
    keydown: function(e) {
        const globe = this.el.components.globe.globe;
        const deltaRad = this.data.deltaRad;
        const deltaScale = this.data.deltaScale;
        if (e.key == "a") {
            q = new THREE.Quaternion()
            q.setFromAxisAngle(new THREE.Vector3(0,1,0), -deltaRad);
            globe.applyQuaternion(q);
        }else if(e.key == "d") {
            q = new THREE.Quaternion()
            q.setFromAxisAngle(new THREE.Vector3(0,1,0), deltaRad);
            globe.applyQuaternion(q);
        }else if(e.key == "w") {
            q = new THREE.Quaternion()
            q.setFromAxisAngle(new THREE.Vector3(1, 0, 0), deltaRad);
            globe.applyQuaternion(q);
        }else if(e.key == "s") {
            q = new THREE.Quaternion()
            q.setFromAxisAngle(new THREE.Vector3(1, 0, 0), -deltaRad);
            globe.applyQuaternion(q);
        }
        else if(e.key == "e") {
            q = new THREE.Quaternion()
            q.setFromAxisAngle(new THREE.Vector3(0, 0, 1), deltaRad);
            globe.applyQuaternion(q);
        }else if(e.key == "q") {
            q = new THREE.Quaternion()
            q.setFromAxisAngle(new THREE.Vector3(0, 0, 1), -deltaRad);
            globe.applyQuaternion(q);
        }else if(e.key == "r"){
            globe.scale.multiplyScalar(1/deltaScale);
        }else if(e.key == "f"){
            globe.scale.multiplyScalar(deltaScale);
        }
    }
});